const express = require('express');
const router = express.Router();

const { isAuthenticated } = require('../helpers/auth');

// Requerir model back
const Note = require('../models/Note');
const Competencias = require('../models/Competencias');
const Formulario = require('../models/Formulario');

router.get('/notes/add', isAuthenticated, (req, res) => {
    res.render('notes/new-note');
});
router.get('/notes/participantes', isAuthenticated, async(req, res) => {

    const form = await Formulario.find();

    res.render('notes/participantes', { form });
});




// Rutas de back

router.post('/notes/new-note', async(req, res) => {
    const { name, url, desc, edades } = req.body;
    const errors = [];

    if (!name) {
        errors.push({
            text: 'Por favor insertar un Nombre'
        });
    }
    if (!edades) {
        errors.push({
            text: 'Por favor insertar un para que edad dispone este curso'
        });
    }
    if (!url) {
        errors.push({
            text: 'Por favor insertar una url de la imagen'
        });
    }
    if (!desc) {
        errors.push({
            text: 'Por favor insertar una descripción'
        });
    }
    if (errors.length > 0) {
        res.render('notes/new-note', {
            errors,
            name,
            url,
            desc,
            edades
        });
    } else {
        const newNotes = new Competencias({
            name,
            url,
            desc,
            edades
        });
        // Esta line me permite saber el enlace de la nota con el user
        newNotes.user = req.user.id;
        await newNotes.save();
        req.flash('success_msg', 'Se registro el producto');
        res.redirect('/notes');
    }
});

// Esta ruta va a recibir notas de la db
router.get('/notes', isAuthenticated, async(req, res) => {
    const notes = await Competencias.find({ user: req.user.id }).sort({ date: 'desc' });

    // Con esto va a renderizar la pag y pasarle las notas
    // res.render('notes/all-notes');
    res.render('notes/all-notes', { notes });
});


// Esta ruta te permite traer la vista para editar, debe llamar a un formulario

router.get('/notes/edit/:id', isAuthenticated, async(req, res) => {
    const note = await Competencias.findById(req.params.id);
    res.render('notes/edit-note', { note });
});
router.get('/notes/editP/:id', isAuthenticated, async(req, res) => {
    const note = await Formulario.findById(req.params.id);
    res.render('notes/edit-par', { note });
});

// Esta ruta hace la peticion put para actualizar la db

router.put('/notes/edit-noteP/:id', isAuthenticated, async(req, res) => {
    const { name, comp, estado } = req.body;
    await Formulario.findByIdAndUpdate(req.params.id, { name, comp, estado });
    req.flash('success_msg', 'El estado de la competencia se actualizó correctamente');
    res.redirect('/notes/participantes');
});
router.put('/notes/edit-note/:id', isAuthenticated, async(req, res) => {
    const { name, edades, url, desc } = req.body;
    await Competencias.findByIdAndUpdate(req.params.id, { name, edades, url, desc });
    req.flash('success_msg', 'La competencia se actualizó correctamente');
    res.redirect('/notes');
});

// para eliminar la nota

router.delete('/notes/delete/:id', isAuthenticated, async(req, res) => {
    await Competencias.findByIdAndDelete(req.params.id);
    req.flash('success_msg', 'La competencia se eliminó correctamente');
    res.redirect('/notes');
});

module.exports = router;