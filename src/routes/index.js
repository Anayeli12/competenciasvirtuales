const express = require('express');
const router = express.Router();
const Competencias = require('../models/Competencias');
const Formulario = require('../models/Formulario');
const Img = require('../models/Img');


const { isAuthenticated } = require('../helpers/auth');

router.get('/', async(req, res) => {
    const Comp = await Competencias.find().sort({ date: 'desc' });

    res.render('index', { Comp });
});
// router.get('/start', (req, res) => {
//     res.render('start');
// });
// router.get('/plays', isAuthenticated, (req, res) => {
//     res.render('plays');
// });
router.get('/about', (req, res) => {
    res.render('about');
});
router.get('/contact', (req, res) => {
    res.render('contact');
});
router.get('/registro', async(req, res) => {
    // const note = await Note.find();
    const form = await Formulario.find().sort({ date: 'desc' });
    // const img = await Img.find();

    // res.render('product'), { notes };
    res.render('product', { form });
});


module.exports = router;