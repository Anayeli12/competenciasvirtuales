const mongoose = require('mongoose');

require('./config/config');

mongoose.connect(process.env.urlDB, {
        useCreateIndex: true,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    })
    .then(db => console.log('DB conectada'))
    .catch(err => console.error(err));