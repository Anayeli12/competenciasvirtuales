const mongoose = require('mongoose');
const { Schema } = mongoose;

const CompetenciasSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    desc: {
        type: String,
        required: true
    },
    user: {
        type: String
    },
    edades: {
        type: String
    },
    date: {
        type: Date,
        default: Date.now
    },
    url: {
        type: String,
    }

});

module.exports = mongoose.model('Competencias', CompetenciasSchema);