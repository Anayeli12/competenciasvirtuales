const mongoose = require('mongoose');
const { Schema } = mongoose;

const FormularioSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    iden: {
        type: Number,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    comp: {
        type: String,
        required: true
    },
    edades: {
        type: String,
        required: true
    },
    estado: {
        type: String,
    },

    date: {
        type: Date,
        default: Date.now
    },


});

module.exports = mongoose.model('Formulario', FormularioSchema);